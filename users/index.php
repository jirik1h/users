<?php
$msg = "";
if(isset($_POST['form_send'])){
	$service_url = 'localhost:8080/users/api/user/create.php';
	$curl = curl_init($service_url);
	$curl_post_data = array(
			'name' => $_POST['name'],
			'email' => $_POST['email'],
			'role' => $_POST['role'],
			'pwd' => $_POST['pwd'],
			'pwd_conf' => $_POST['pwd_conf'],
	);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($curl_post_data));
	$curl_response = curl_exec($curl);
	curl_close($curl);
	$msg = $curl_response;
}

$service_url = 'localhost:8080/users/api/user/read.php';

$curl = curl_init($service_url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$curl_response_users = curl_exec($curl);
curl_close($curl);
$users_list = json_decode($curl_response_users);

?>
<html>
	<body>
	<h1>
		Vkládání nových uživatelů
	</h1>
	<div>
		<div style="font-size:25px;"><?php echo $msg;?></div>
		<form method="post">
			<input type="hidden" name="form_send" value="1">
			<label>
				Jméno
				<input type="text" name="name">
			</label>
			<br>
			<label>
				Email
				<input type="email" name="email">
			</label>
			<br>
			<label>
				Heslo
				<input type="password" name="pwd">
			</label>
			<br>
			<label>
				Heslo znovu
				<input type="password" name="pwd_conf">
			</label>
			<br>
			<label>
				Oprávnění
				<select name="role">
					<option value="user" selected>Uživatel</option>
					<option value="admin">Admin</option>
				</select>
			</label>
			<br>
			<input type="submit" value="Uložit">
		</form>
	</div>
	<h2>
		Seznam uživatelů (hesla jsem nešifroval kvůli kontrole)
	</h2>
	<div>
	<style>
	td {
		border: 1px solid black;
		padding: 5px;
	}
	</style>
		<table cellspacing="0">
		<tr>
			<td>ID</td>
			<td>Jméno</td>
			<td>Email</td>
			<td>Heslo</td>
			<td>Role</td>
		</tr>
		<?php
			foreach($users_list->records as $single_user){
				echo "<tr>";
				echo "<td>".$single_user->user_id."</td>";
				echo "<td>".$single_user->name."</td>";
				echo "<td>".$single_user->email."</td>";
				echo "<td>".$single_user->pwd."</td>";
				echo "<td>".$single_user->role."</td>";
				echo "</tr>";
			}
		?>
		</table>
	</div>
	</body>
</html>