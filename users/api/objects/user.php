<?php
class User{
 
    private $conn;
    private $table_name = "users";
 
    public $id;
    public $name;
    public $email;
    public $role;
	public $pwd;
	public $pwd_conf;
 
    public function __construct($db){
        $this->conn = $db;
    }
	
	function create(){
		$res = $this->conn->query("SELECT * FROM " . $this->table_name . " WHERE email='" . $this->email ."'");
		$cnt = $res->rowCount();
		if( $cnt > 0 ){
			return "Zadaný email je již registrovaný!";
		}
		if( empty($this->email) || (filter_var($this->email, FILTER_VALIDATE_EMAIL) === false) ){
			return "Špatně zadaný email!";
		}
		if( empty($this->role) || !in_array($this->role,array('admin','user')) ){
			return "Špatně zadaná role!";
		}
		if( empty($this->name) ){
			return "Špatně zadané jméno!";
		}
		if( $this->pwd != $this->pwd_conf ){
			return "Hesla se neshodují!";
		}
		if( strlen($this->pwd) < 6 || strlen($this->pwd) > 16 ){
			return "Heslo musí obsahovat minimálně 6 a maximálně 16 znaků!";
		}
		if ( preg_match('/\s/',$this->pwd) ){
			return "Heslo nesmí obsahovat bílé znaky jako jsou mezery apod!";
		}
		
		
		$this->name = trim(strip_tags($this->name));
		$this->email = trim(strip_tags($this->email));
		$this->pwd = trim(strip_tags($this->pwd));
	 
		$query = "INSERT INTO
			" . $this->table_name . " (name, email, role, pwd) 
			VALUES ('".$this->name."','".$this->email."','".$this->role."','".$this->pwd."')";
	 
		$res = $this->conn->prepare($query);
		if($res->execute()){
			return "Uživatel byl vytvořen.";
		}
	 
		return "Nepodařilo se vytvořit uživatele!";
		 
	}
	
	function read(){
		$res = $this->conn->query("SELECT * FROM " . $this->table_name); 	 
		return $res;
	}
}
?>