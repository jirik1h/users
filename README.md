# Registrace uživatelů

Jednoduchá stránka pro registraci uživatelů a zobrazení již registrovaných uživatelů. Hesla nejsou v databázi zašifrovaná kvůli kontrole správnosti ukládání. Všechny položky ve formuláři jsou povinné.

## Využité technologie

Projekt je třeba spustit na localhostu. Konkrétní adresa je localhost:8080/users/index.php. Api je ve stejné složce v adresáři api. Využité technologie jsou HTML, PHP, Apache, MySQL. SQL pro import databáze je níže. Databáze se jmenuje users_api.

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Čtv 27. zář 2018, 16:20
-- Verze serveru: 10.1.13-MariaDB
-- Verze PHP: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `users_api`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `user_id` int(5) NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `pwd` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `email` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `role` enum('admin','user') CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_czech_ci;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`user_id`, `name`, `pwd`, `email`, `role`) VALUES
(1, 'Jiří Holubář', 'asdfghjkl', 'holubar@gmail.com', 'admin');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
